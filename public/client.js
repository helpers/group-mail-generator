/* global axios */

function createLink(email, subject, content) {
  const e = email;
  const s = encodeURIComponent(subject);
  const c = encodeURIComponent(content);

  return `mailto:${e}?subject=${s}&body=${c}`;
}

$(document).ready(async () => {
  const res = await axios.get('/data');
  console.log(res.data);
  const rows = res.data.map(
    (e) => `
  <tr>
  <td>${e.email}</td>
  <td>${e.subject}</td>
  <td>${e.content.replace(/\n/g, '<br>')}</td>
  <td><a href="${createLink(e.email, e.subject, e.content)}">email</a></td>
  <tr>`,
  );
  $('tbody', '#table').append(rows.join(''));
});

# Email generator based on Templates

This project allows for emails to be generated based on templates and spreadsheet based lists.

Emails are not sent from this project. Instead URIs of `malto://` links are rendered within a website to allow the user to open their configured email client for sending the emails content.

# To run

- Install `nodejs` and `yarn`
    - https://yarnpkg.com/lang/en/
    - https://nodejs.org/en/

- run `yarn`
- run `node main.js`
- Open Browser: http://localhost:8000/

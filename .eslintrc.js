module.exports = {
  env: {
    browser: true,
    es6: true,
    jquery: true,
  },
  extends: ["airbnb", "prettier"],
  plugins: ["prettier"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly"
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module"
  },
  rules: {
    "prettier/prettier": ["error"],
    "prefer-template": ["error"],
    "func-names": [0]
  }
};

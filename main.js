const papa = require('papaparse');
const fs = require('fs');
const express = require('express');

const app = express();

const contentTempalte = fs.readFileSync('./templates/content.txt').toString();
const subjectTemplate = fs.readFileSync('./templates/subject.txt').toString();
const dataSet = papa.parse(
  fs
    .readFileSync('./spreadsheet/data.csv')
    .toString()
    .trim(),
  { header: true },
);

console.log(contentTempalte);
console.log(subjectTemplate);
console.log(dataSet);

const items = dataSet.data.map((entry) => {
  let blankContent = contentTempalte;
  let blankSubject = subjectTemplate;
  Object.keys(entry).forEach((e) => {
    blankContent = blankContent.replace(`$${e}`, entry[e]);
  });

  Object.keys(entry).forEach((e) => {
    blankSubject = blankSubject.replace(`$${e}`, entry[e]);
  });

  return {
    content: blankContent,
    subject: blankSubject,
    email: entry.EMAIL,
  };
});

console.log(items);

app.get('/data', (req, res) => {
  res.send(items);
});

app.use(express.static('public'));

app.listen(8000, () => 'listneing on port 8000');
